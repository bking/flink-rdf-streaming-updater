#!/bin/bash

set -ex;

# Configure Flink version
FLINK_VERSION=$(ls lib/flink-dist-*.jar | sed -e 's#lib/flink-dist-\(.*\)\.jar#\1#')
FLINK_TGZ_URL="https://archive.apache.org/dist/flink/flink-$FLINK_VERSION/flink-$FLINK_VERSION-bin-scala_2.12.tgz"
FLINK_ASC_URL="https://archive.apache.org/dist/flink/flink-$FLINK_VERSION/flink-$FLINK_VERSION-bin-scala_2.12.tgz.asc"
GPG_KEY="A5F3BCE4CBE993573EC5966A65321B8382B219AF"
CHECK_GPG=${CHECK_GPG:-false}

curl --fail --show-error -Lo flink.tgz "$FLINK_TGZ_URL";

if [ "$CHECK_GPG" = "true" ]; then
    curl --fail --show-error -Lo flink.tgz.asc "$FLINK_ASC_URL";
    cat flink.tgz.asc
    export GNUPGHOME="$(mktemp -d)";
    for server in ha.pool.sks-keyservers.net $(shuf -e \
                            hkp://p80.pool.sks-keyservers.net:80 \
                            keyserver.ubuntu.com \
                            hkp://keyserver.ubuntu.com:80 \
                            pgp.mit.edu) ; do
        gpg --batch --keyserver "$server" --recv-keys "$GPG_KEY" && break || : ;
    done && gpg --batch --verify flink.tgz.asc flink.tgz;
    gpgconf --kill all;
    rm -rf "$GNUPGHOME" flink.tgz.asc;
fi;

mkdir /tmp/flink
tar -C /tmp/flink -xf flink.tgz --strip-components=1;
mv /tmp/flink/lib/flink-scala* /tmp/flink-scala.jar
rm -rf /tmp/flink
rm -rf flink.tgz
