#!/bin/sh

# TODO: find a better way to test, standalone-job.sh requires the job in usrlib but we don't want to put it there to avoid
# having classpath issues as the k8s-operator requires the jarUri to be passed
mkdir /opt/flink/usrlib
ln -s /opt/flink/streaming-updater-producer.jar /opt/flink/usrlib

/opt/flink/bin/standalone-job.sh start-foreground \
    --job-classname org.wikidata.query.rdf.updater.UpdaterJob  \
    --checkpoint_dir swift:///tmp  \
    --print_execution_plan true  \
    --hostname www.wikidata.org  \
    --job_name my_test_job_name  \
    --brokers one,two  \
    --output_topic my_output_topic  \
    --output_topic_partition 0  \
    --entity_namespaces 0,120  \
    --uris_scheme wikidata  \
    --rev_create_topic mediawiki.revision-create  \
    --page_delete_topic mediawiki.page-delete  \
    --suppressed_delete_topic mediawiki.page-suppress  \
    --page_undelete_topic mediawiki.page-undelete  \
    --consumer_group my_consumer_group 2>&1 | tee -a /tmp/test_log | grep 'Filtered(mediawiki.page-delete == www.wikidata.org)' > /dev/null

GREP_SUCCESS=$?

cat /tmp/test_log
if [ $GREP_SUCCESS -ne 0 ]; then
	echo FAILURE
	exit 1
else
	echo SUCCESS
fi
